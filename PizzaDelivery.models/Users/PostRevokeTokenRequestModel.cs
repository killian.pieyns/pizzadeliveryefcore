﻿namespace PizzaDelivery.models.Users
{
    public class PostRevokeTokenRequestModel
    {
        public string Token { get; set; }
    }
}
