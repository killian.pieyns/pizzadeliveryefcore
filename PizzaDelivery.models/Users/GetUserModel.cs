﻿using System;
using System.Collections.Generic;

namespace PizzaDelivery.models.Users
{
    public class GetUserModel : BaseUserModel
    {
        public Guid Id { get; set; }

        public ICollection<string> Roles { get; set; }
    }
}
