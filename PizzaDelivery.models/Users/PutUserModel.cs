﻿using System.Collections.Generic;

namespace PizzaDelivery.models.Users
{
    public class PutUserModel : BaseUserModel
    {
        public ICollection<string> Roles { get; set; }
    }
}
