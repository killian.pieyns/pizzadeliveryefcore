﻿using System.ComponentModel.DataAnnotations;

namespace PizzaDelivery.models.Users
{
    public class BaseUserModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Voornaam { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Familienaam { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
