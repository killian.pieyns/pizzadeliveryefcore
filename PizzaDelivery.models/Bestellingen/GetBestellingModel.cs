﻿using PizzaDelivery.models.Pizzas;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaDelivery.models.Bestellingen
{
    public class GetBestellingModel : BaseBestellingModel
    {
        public Guid Id { get; set; }
        public Guid? BereiderId { get; set; }
        public Guid? VervoerderId { get; set; }
        public Guid? VoertuigId { get; set; }
        public string Klant { get; set; }
        public string Bereider { get; set; }
        public string Vervoerder { get; set; }
        public List<GetPizzaModel> Pizzas { get; set; }
        public decimal TotaalPrijs { get; set; }
        public DateTime Besteldatum { get; set; }
        public DateTime Leverdatum { get; set; }
    }
}
