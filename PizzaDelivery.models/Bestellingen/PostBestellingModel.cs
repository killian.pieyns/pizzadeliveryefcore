﻿using PizzaDelivery.models.Pizzas;
using PizzaDelivery.models.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaDelivery.models.Bestellingen
{
    public class PostBestellingModel : BaseBestellingModel
    {
        public List<PostPizzaModel> Pizzas { get; set; }
        public DateTime Besteldatum { get; set; }
    }
}
