﻿using PizzaDelivery.models.Pizzas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PizzaDelivery.models.Bestellingen
{
    public class PutBestellingModel : BaseBestellingModel
    {
        public Guid BereiderId { get; set; }
        public Guid? VervoerderId { get; set; }
        public Guid? VoertuigId { get; set; }
        [Required]
        public DateTime TijdstipLevering { get; set; }
        public List<PutPizzaModel> Pizzas { get; set; }
    }
}