﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PizzaDelivery.models.Pizzas
{
    public class PutPizzaModel : BasePizzaModel
    {
        [Required]
        [Range(0, 2)]
        public int Formaat { get; set; }

        [Required]
        [RegularExpression(@"^[1-9]\d{0,1}$")]
        public int Aantal { get; set; }
        public Guid StandaardPizzaId { get; set; }
    }
}
