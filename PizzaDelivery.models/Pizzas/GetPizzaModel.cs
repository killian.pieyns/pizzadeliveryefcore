﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaDelivery.models.Pizzas
{
    public class GetPizzaModel : BasePizzaModel
    {
        public string Naam { get; set; }
        public int Aantal { get; set; }
        public string Formaat { get; set; }
        public decimal Eenheidsprijs { get; set; }
        public decimal Subtotaalprijs { get; set; }
    }
}
