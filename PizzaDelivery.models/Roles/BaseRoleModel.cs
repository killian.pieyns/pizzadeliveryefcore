﻿using System.ComponentModel.DataAnnotations;

namespace mediatheek.models.Roles
{
    public class BaseRoleModel
    {
        [Required]
        public string Name { get; set; }

        [StringLength(255)]
        public string Omschrijving { get; set; }
    }
}
