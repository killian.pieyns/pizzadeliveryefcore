﻿using System;

namespace mediatheek.models.Roles
{
    public class GetRoleModel : BaseRoleModel
    {
        public Guid Id { get; set; }
    }
}
