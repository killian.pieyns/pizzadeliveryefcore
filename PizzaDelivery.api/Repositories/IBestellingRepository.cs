﻿using PizzaDelivery.models.Bestellingen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Repositories
{
    public interface IBestellingRepository
    {
        Task<List<GetBestellingModel>> GetBestellingen(Guid userId);
        Task<GetBestellingModel> GetBestelling(Guid userId, Guid id);
        Task<GetBestellingModel> PostBestelling(Guid userId, PostBestellingModel postBestellingModel);
        Task PutBestelling(Guid userId, Guid id, PutBestellingModel putBestellingModel);
        Task DeleteBestelling(Guid userId, Guid id);
    }
}
