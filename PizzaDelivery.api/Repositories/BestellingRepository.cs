﻿using PizzaDelivery.Api.Entities;
using PizzaDelivery.Api.Exceptions;
using PizzaDelivery.models.Bestellingen;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using PizzaDelivery.models.Pizzas;

namespace PizzaDelivery.Api.Repositories
{
    public class BestellingRepository : IBestellingRepository
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ClaimsPrincipal _user;
        private readonly PizzaDeliveryContext _context;
        public BestellingRepository(PizzaDeliveryContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _user = _httpContextAccessor.HttpContext.User;
        }


        /// <inheritdoc/>
        public async Task<List<GetBestellingModel>> GetBestellingen(Guid userId)
        {
            try
            {
                User user = await GetUser(userId, "GetBestellingen");

                List<GetBestellingModel> bestellingen = await _context.Bestellingen
                    .Include(x => x.Vervoerder)
                    .Include(x => x.Pizzas)
                    .ThenInclude(x => x.StandaardPizza)
                    .Select(x => new GetBestellingModel
                    {
                        Id = x.Id,
                        KlantId = x.KlantId,
                        BereiderId = x.BereiderId == null ? null : x.BereiderId,
                        VervoerderId = x.VervoerderId,
                        VoertuigId = x.VoertuigId,
                        Klant = $"{x.Klant.Familienaam} {x.Klant.Voornaam}",
                        Bereider = $"{x.Bereider.Familienaam} {x.Bereider.Voornaam}",
                        Vervoerder = $"{x.Vervoerder.Familienaam} {x.Vervoerder.Voornaam}",
                        Besteldatum = x.TijdstipBestelling,
                        Leverdatum = x.TijdstipLevering,
                        Pizzas = x.Pizzas.Select(x => new GetPizzaModel
                        {
                            Naam = x.StandaardPizza.Naam,
                            Aantal = x.Aantal,
                            Formaat = x.Formaat.ToString(),
                            Eenheidsprijs =
                                x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                                 x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium : x.StandaardPizza.PrijsGroot,
                            Subtotaalprijs =
                                x.Aantal * (x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                                 x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium : x.StandaardPizza.PrijsGroot)

                        }).ToList(),
                        TotaalPrijs = x.Pizzas.Sum(x =>
                            x.Aantal * (x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                            x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium : x.StandaardPizza.PrijsGroot))
                    })
                    .AsNoTracking()
                    .ToListAsync();

                if (bestellingen.Count == 0)
                {
                    throw new CollectionException("No bestellingen found.", this.GetType().Name, "GetBestellingen", "404");
                }

                if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                _user.IsInRole("Klant"))
                {
                    if (_user.Identity.Name != userId.ToString())
                    {
                        throw new ForbiddenException("Forbidden to get this user bestelling.", this.GetType().Name, "GetBestellingen", "403");
                    }
                    else if (_user.Identity.Name == userId.ToString())
                    {
                        return bestellingen.Where(x => x.KlantId == user.Id).ToList();
                    }
                }

                return bestellingen;
            }
            catch (PizzaDeliveryException e)
            {
                throw e;
            }
        }




        /// <inheritdoc/>
        public async Task<GetBestellingModel> GetBestelling(Guid userId, Guid id)
        {
            try
            {
                User user = await GetUser(userId, "GetBestelling");


                GetBestellingModel bestellingen = await _context.Bestellingen
                    .Include(x => x.Pizzas)
                    .ThenInclude(x => x.StandaardPizza)
                    .Where(x => x.Id == id && x.KlantId == userId)
                    .Select(x => new GetBestellingModel
                    {
                        Id = x.Id,
                        KlantId = x.KlantId,
                        BereiderId = x.BereiderId,
                        VervoerderId = x.VervoerderId,
                        VoertuigId = x.VoertuigId,
                        Klant = $"{x.Klant.Familienaam} {x.Klant.Voornaam}",
                        Bereider = $"{x.Bereider.Familienaam} {x.Bereider.Voornaam}",
                        Vervoerder = $"{x.Vervoerder.Familienaam} {x.Vervoerder.Voornaam}",
                        Besteldatum = x.TijdstipBestelling,
                        Leverdatum = x.TijdstipLevering,
                        Pizzas = x.Pizzas.Select(x => new GetPizzaModel
                        {
                            Naam = x.StandaardPizza.Naam,
                            Aantal = x.Aantal,
                            Formaat = x.Formaat.ToString(),
                            Eenheidsprijs =
                                x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                                 x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium :
                                 x.Formaat == FormaatEnum.Groot ? x.StandaardPizza.PrijsGroot : 0.0M,
                            Subtotaalprijs =
                                x.Aantal * (x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                                 x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium :
                                 x.Formaat == FormaatEnum.Groot ? x.StandaardPizza.PrijsGroot : 0.0M),

                        }).ToList(),
                        TotaalPrijs = x.Pizzas.Sum(x =>
                            x.Aantal * (x.Formaat == FormaatEnum.Klein ? x.StandaardPizza.PrijsKlein :
                            x.Formaat == FormaatEnum.Medium ? x.StandaardPizza.PrijsMedium :
                            x.Formaat == FormaatEnum.Groot ? x.StandaardPizza.PrijsGroot : 0.0M)),
                    })
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (bestellingen == null)
                {
                    throw new EntityException("bestellingen not found.", this.GetType().Name, "Getbestelling", "404");
                }

                if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                _user.IsInRole("Klant") && _user.Identity.Name != userId.ToString())
                {
                     throw new ForbiddenException("Forbidden to get this user bestelling.", this.GetType().Name, "GetBestellingen", "403");
                }

                return bestellingen;
            }
            catch (PizzaDeliveryException e)
            {
                throw e;
            }
        }



        /// <inheritdoc/>
        public async Task<GetBestellingModel> PostBestelling(Guid userId, PostBestellingModel postBestellingModel)
        {
            try
            {
                User user = await GetUser(userId, "PostBestelling");

                EntityEntry<Bestelling> resultBestelling = await _context.Bestellingen.AddAsync(new Bestelling
                {
                    KlantId = postBestellingModel.KlantId,
                    TijdstipBestelling = postBestellingModel.Besteldatum
                });



                foreach (PostPizzaModel postPizzaModel in postBestellingModel.Pizzas)
                {
                    EntityEntry<Pizza> resultPizza = await _context.Pizzas.AddAsync(new Pizza
                    {
                        Aantal = postPizzaModel.Aantal,
                        Formaat = (FormaatEnum)postPizzaModel.Formaat,
                        BestellingId = resultBestelling.Entity.Id,
                        StandaardPizzaId = postPizzaModel.StandaardPizzaId
                    });
                }

                await _context.SaveChangesAsync();

                return await GetBestelling(userId, resultBestelling.Entity.Id);
            }
            catch (PizzaDeliveryException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new DatabaseException(e.InnerException.Message, this.GetType().Name, "PostBestelling", "400");
            }
        }

        /*
         * 
         * ID: 42c0d2bb-2de2-4836-c29c-08d8bb0151b3
         * 
         * BODY:
         * 
            {
              "klantId": "9025a83d-9c27-40e0-eaa9-08d8bb0146c0",
              "bereiderId": "fadd28aa-ebe0-4de4-eaa8-08d8bb0146c0",
              "vervoerderId": "8e6ae62a-f3f8-4b1a-eaa7-08d8bb0146c0",
              "voertuigId": "9b0931a5-c8b1-42e0-2d85-08d8bb014f95",
              "besteldatum": "2021-01-14T17:30:00",
              "leverdatum": "2021-01-14T18:00:00",
              "aantal": [
                5
              ],
              "formaat": [
                2
              ],
              "standaardPizzas": [
                "b2078aa5-415e-4677-8e00-08d8bb015117"
              ]
            }
         */

        /// <inheritdoc/>
        public async Task PutBestelling(Guid userId, Guid id, PutBestellingModel putBestellingModel)
        {
            if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                !(_user.IsInRole("Beheerder") || _user.IsInRole("Administrator") || _user.IsInRole("Bereider")) &&
                _user.Identity.Name != userId.ToString())
            {
                throw new ForbiddenException("Forbidden to get this user bestelling.", this.GetType().Name, "GetBestellingen", "403");
            }

            try
            {
                User user = await GetUser(userId, "PutBestelling");

                Bestelling bestelling = await _context.Bestellingen
                    .Include(x => x.Pizzas)
                    .ThenInclude(x => x.PizzaToppings)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (bestelling == null)
                {
                    throw new EntityException("Bestelling not found", this.GetType().Name, "PutBestelling", "404");
                }

                //check Id
                // *to implement*

                bestelling.BereiderId = putBestellingModel.BereiderId;
                bestelling.VervoerderId = putBestellingModel.VervoerderId;
                bestelling.VoertuigId = putBestellingModel.VoertuigId;
                bestelling.TijdstipLevering = putBestellingModel.TijdstipLevering;

                if (putBestellingModel.Pizzas != null && putBestellingModel.Pizzas.Count > 0)
                {
                    _context.PizzaToppings.RemoveRange(bestelling.Pizzas.SelectMany(x => x.PizzaToppings));
                    _context.Pizzas.RemoveRange(bestelling.Pizzas);
                    foreach (PutPizzaModel putPizzaModel in putBestellingModel.Pizzas)
                    {
                        EntityEntry<Pizza> resultPizza = await _context.Pizzas.AddAsync(new Pizza
                        {
                            Aantal = putPizzaModel.Aantal,
                            Formaat = (FormaatEnum)putPizzaModel.Formaat,
                            BestellingId = bestelling.Id,
                            StandaardPizzaId = putPizzaModel.StandaardPizzaId
                        });
                    }
                }
                
                await _context.SaveChangesAsync();

            }
            catch (PizzaDeliveryException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new DatabaseException(e.InnerException.Message, this.GetType().Name, "PutBestelling", "400");
            }
        }

        /// <inheritdoc/>
        public async Task DeleteBestelling(Guid userId, Guid id)
        {
            try
            {
                User user = await GetUser(userId, "Deletebestelling");

                Bestelling bestelling = await _context.Bestellingen
                    .Include(x => x.Pizzas)
                    .ThenInclude(x => x.PizzaToppings)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (bestelling == null)
                {
                    throw new EntityException("Bestelling not found.", this.GetType().Name, "Deletebestelling", "404");
                }

                _context.PizzaToppings.RemoveRange(bestelling.Pizzas.SelectMany(x => x.PizzaToppings));
                _context.Pizzas.RemoveRange(bestelling.Pizzas);
                _context.Bestellingen.Remove(bestelling);

                await _context.SaveChangesAsync();
            }
            catch (PizzaDeliveryException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new DatabaseException(e.InnerException.Message, this.GetType().Name, "Deletebestelling", "400");
            }
        }

        private async Task<User> GetUser(Guid userId, string sourceMethod)
        {
            User user = await _context.Users.AsNoTracking().FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new EntityException("User not found.", this.GetType().Name, sourceMethod, "404");
            }

            return user;
        }
    }
}
