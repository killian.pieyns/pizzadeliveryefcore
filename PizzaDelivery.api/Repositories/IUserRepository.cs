﻿using PizzaDelivery.models.RefreshTokens;
using PizzaDelivery.models.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Repositories
{
    public interface IUserRepository
    {
        Task<List<GetUserModel>> GetUsers();
        Task<GetUserModel> GetUser(Guid id);
        Task<GetUserModel> PostUser(PostUserModel postUserModel);
        Task PutUser(Guid id, PutUserModel putUserModel);
        Task PatchUser(Guid id, PatchUserModel patchUserModel);
        Task DeleteUser(Guid id);
        Task<List<GetRefreshTokenModel>> GetUserRefreshTokens(Guid id);

        // JWT methods

        Task<PostAuthenticateResponseModel> Authenticate(PostAuthenticateRequestModel postAuthenticateRequestModel, string ipAddress);
        Task<PostAuthenticateResponseModel> RefreshToken(string token, string ipAddress);
        Task RevokeToken(string token, string ipAddress);
    }
}
