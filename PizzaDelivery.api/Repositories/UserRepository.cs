﻿using PizzaDelivery.Api.Entities;
using PizzaDelivery.Api.Exceptions;
using PizzaDelivery.Api.Helpers;
using PizzaDelivery.models.RefreshTokens;
using PizzaDelivery.models.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly PizzaDeliveryContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly AppSettings _appSettings;
        private readonly ClaimsPrincipal _user;

        public UserRepository(
            PizzaDeliveryContext context, 
            UserManager<User> userManager, 
            RoleManager<Role> roleManager,
            SignInManager<User> signInManager,
            IHttpContextAccessor httpContextAccessor,
            IOptions<AppSettings> appSettings)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _appSettings = appSettings.Value;
            _user = _httpContextAccessor.HttpContext.User;
        }

        /// <inheritdoc/>
        public async Task<List<GetUserModel>> GetUsers()
        {
            List<GetUserModel> users = await _context.Users
                .Include(x => x.UserRoles)
                .Select(x => new GetUserModel
                {
                    Id = x.Id,
                    Voornaam = x.Voornaam,
                    Familienaam = x.Familienaam,
                    Email = x.Email,
                    Roles = x.UserRoles.Select(x => x.Role.Name).ToList()
                })
                .AsNoTracking()
                .ToListAsync();

            if (users.Count == 0)
            {
                throw new CollectionException("No users found.", this.GetType().Name, "GetUsers", "404");
            }

            return users;
        }

        /// <inheritdoc/>
        public async Task<GetUserModel> GetUser(Guid id)
        {
            if (_user.Claims.Count(x => x.Type.Contains("role")) == 1 && 
                (_user.IsInRole("Beheerder") || _user.IsInRole("Administrator") || _user.IsInRole("Vervoerder")) &&
                _user.Identity.Name != id.ToString())
            {
                throw new ForbiddenException("Forbidden to get this user details.", this.GetType().Name, "GetUser", "403");
            }

            GetUserModel user = await _context.Users
                .Include(x => x.UserRoles)
                .Include(x => x.RefreshTokens)
                .Select(x => new GetUserModel
                {
                    Id = x.Id,
                    Voornaam = x.Voornaam,
                    Familienaam = x.Familienaam,
                    Email = x.Email,
                    Roles = x.UserRoles.Select(x => x.Role.Name).ToList()
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityException("User not found.", this.GetType().Name, "GetUser", "404");
            }

            return user;
        }

        /// <inheritdoc/>
        public async Task<GetUserModel> PostUser(PostUserModel postUserModel)
        {
            User user = new User
            {
                UserName = postUserModel.Email,
                Voornaam = postUserModel.Voornaam,
                Familienaam = postUserModel.Familienaam,
                Email = postUserModel.Email,
            };

            IdentityResult userResult = await _userManager.CreateAsync(user, postUserModel.Password);

            if (!userResult.Succeeded)
            {
                throw new IdentityException(userResult.Errors.First().Description, this.GetType().Name, "PostUser", "400");
            }

            try
            {
                if (postUserModel.Roles == null)
                {
                    await _userManager.AddToRoleAsync(user, "Klant");
                }
                else
                {
                    await _userManager.AddToRolesAsync(user, postUserModel.Roles);
                }
            }
            catch (Exception e)
            {
                await _userManager.DeleteAsync(user);
                throw new IdentityException(e.Message, this.GetType().Name, "PostUser", "400");
            }

            return await GetUser(user.Id);
        }

        /// <inheritdoc/>
        public async Task PutUser(Guid id, PutUserModel putUserModel)
        {
            if (_user.Claims.Count(x => x.Type.Contains("role")) == 1 &&
                (_user.IsInRole("Beheerder") || _user.IsInRole("Administrator")) &&
                _user.Identity.Name != id.ToString())
            {
                throw new ForbiddenException("Forbidden to get this user details.", this.GetType().Name, "GetUser", "403");
            }

            try
            {
                if (putUserModel.Roles == null)
                {
                    throw new IdentityException("User must have at least one role", this.GetType().Name, "PutUser", "400");
                }

                User user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);

                if (user == null)
                {
                    throw new EntityException("User not found.", this.GetType().Name, "PutUser", "404");
                }

                await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));

                user.Voornaam = putUserModel.Voornaam;
                user.Familienaam = putUserModel.Familienaam;
                user.Email = putUserModel.Email;

                await _userManager.AddToRolesAsync(user, putUserModel.Roles);

                await _userManager.UpdateAsync(user);
            }
            catch (PizzaDeliveryException)
            {
                throw;
            }
            catch (Exception e)
            {
                if (e.GetType().Name.Equals("InvalidOperationException")) {
                    throw new IdentityException(e.Message, this.GetType().Name, "PutUser", "400");
                }

                throw new DatabaseException(e.InnerException.Message, this.GetType().Name, "PutUser", "400");
            }
        }

        /// <inheritdoc/>
        public async Task PatchUser(Guid id, PatchUserModel patchUserModel)
        {

            User user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityException("User not found.", this.GetType().Name, "PatchUser", "404");
            }

            IdentityResult result = await _userManager.ChangePasswordAsync(user, patchUserModel.CurrentPassword, patchUserModel.NewPassword);

            if (!result.Succeeded)
            {
                throw new IdentityException(result.Errors.First().Description, this.GetType().Name, "PatchUser", "400");
            }
        }

        /// <inheritdoc/>
        public async Task DeleteUser(Guid id)
        {
            try
            {
                User user = await _context.Users
                    .Include(x => x.BestellingenKlant)
                    .Include(x => x.BestellingenBereider)
                    .Include(x => x.BestellingenVervoerder)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (user == null)
                {
                    throw new EntityException("User not found.", this.GetType().Name, "DeleteUser", "404");
                }

                await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));

                if (user.BestellingenKlant.Count > 0)
                {
                    _context.Bestellingen.RemoveRange(user.BestellingenKlant);
                }
                if (user.BestellingenBereider.Count > 0)
                {
                    _context.Bestellingen.RemoveRange(user.BestellingenBereider);
                }
                if (user.BestellingenVervoerder.Count > 0)
                {
                    _context.Bestellingen.RemoveRange(user.BestellingenVervoerder);
                }

                _ = await _userManager.DeleteAsync(user);
            }
            catch (PizzaDeliveryException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new DatabaseException(e.InnerException.Message, this.GetType().Name, "DeleteUser", "400");
            }
        }

        /// <inheritdoc/>
        public async Task<List<GetRefreshTokenModel>> GetUserRefreshTokens(Guid id)
        {
            List<GetRefreshTokenModel> refreshTokens = await _context.RefreshTokens
                .Where(x => x.UserId == id)
                .Select(x => new GetRefreshTokenModel
                {
                    Id = x.Id,
                    Token = x.Token,
                    Expires = x.Expires,
                    IsExpired = x.IsExpired,
                    Created = x.Created,
                    CreatedByIp = x.CreatedByIp,
                    Revoked = x.Revoked,
                    RevokedByIp = x.RevokedByIp,
                    ReplacedByToken = x.ReplacedByToken,
                    IsActive = x.IsActive,
                })
                .AsNoTracking()
                .ToListAsync();

            if (refreshTokens.Count == 0)
            {
                throw new CollectionException("No refresh tokens found.", this.GetType().Name, "GetUserRefreshTokens", "404");
            }

            return refreshTokens;
        }

        // JWT Methods
        // ===========

        /// <inheritdoc/>
        public async Task<PostAuthenticateResponseModel> Authenticate(PostAuthenticateRequestModel postAuthenticateRequestModel, string ipAddress)
        {
            User user = await _userManager.Users
                .Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.UserName == postAuthenticateRequestModel.UserName);

            if (user == null)
            {
                throw new UserNameException("Invalid username.", this.GetType().Name, "Authenticate", "401");
            }

            // Verify password when user was found by UserName
            SignInResult signInResult = await _signInManager.CheckPasswordSignInAsync(user, postAuthenticateRequestModel.Password, lockoutOnFailure: false);

            if (!signInResult.Succeeded)
            {
                throw new PasswordException("Invalid password.", this.GetType().Name, "Authenticate", "401");
            }

            // Authentication was successful so generate JWT and refresh tokens
            string jwtToken = await GenerateJwtToken(user);
            RefreshToken refreshToken = GenerateRefreshToken(ipAddress);

            // save refresh token
            user.RefreshTokens.Add(refreshToken);

            await _userManager.UpdateAsync(user);

            return new PostAuthenticateResponseModel
            {
                Id = user.Id,
                Voornaam = user.Voornaam,
                Familienaam = user.Familienaam,
                UserName = user.UserName,
                JwtToken = jwtToken,
                RefreshToken = refreshToken.Token,
            };
        }

        /// <inheritdoc/>
        public async Task<PostAuthenticateResponseModel> RefreshToken(string token, string ipAddress)
        {
            User user = await _userManager.Users
                .Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(t => t.Token == token));

            if (user == null)
            {
                throw new TokenException("No user found with this token.", this.GetType().Name, "RefreshToken", "401");
            }

            RefreshToken refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // Refresh token is no longer active
            if (!refreshToken.IsActive)
            {
                throw new RefreshTokenException("Refresh token is no longer active.", this.GetType().Name, "RefreshToken", "401");
            }

            // Replace old refresh token with a new one
            RefreshToken newRefreshToken = GenerateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;

            // Generate new jwt
            string jwtToken = await GenerateJwtToken(user);

            user.RefreshTokens.Add(newRefreshToken);

            await _userManager.UpdateAsync(user);

            return new PostAuthenticateResponseModel
            {
                Id = user.Id,
                Voornaam = user.Voornaam,
                Familienaam = user.Familienaam,
                UserName = user.UserName,
                JwtToken = jwtToken,
                RefreshToken = newRefreshToken.Token,
            };
        }

        /// <inheritdoc/>
        public async Task RevokeToken(string token, string ipAddress)
        {
            User user = await _userManager.Users
                .Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(t => t.Token == token));

            if (user == null)
            {
                throw new TokenException("No user found with this token.", this.GetType().Name, "RefreshToken", "401");
            }

            RefreshToken refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // Refresh token is no longer active
            if (!refreshToken.IsActive)
            {
                throw new RefreshTokenException("Refresh token is no longer active.", this.GetType().Name, "RefreshToken", "401");
            }

            // Revoke token and save
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;

            await _userManager.UpdateAsync(user);
        }

        // JWT helper methods
        // ==================

        private async Task<string> GenerateJwtToken(User user)
        {
            var roleNames = await _userManager.GetRolesAsync(user).ConfigureAwait(false);

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Id.ToString()),
                new Claim("Voornaam", user.Voornaam),
                new Claim("Familienaam", user.Familienaam),
                new Claim("UserName", user.UserName),
            };

            foreach (string roleName in roleNames)
            {
                claims.Add(new Claim(ClaimTypes.Role, roleName));
            }

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.ToArray()),
                Expires = DateTime.UtcNow.AddSeconds(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private static RefreshToken GenerateRefreshToken(string ipAddress)
        {
            using RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddMinutes(20),
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress,
            };
        }
    }
}
