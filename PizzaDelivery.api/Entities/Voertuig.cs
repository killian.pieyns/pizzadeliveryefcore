﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Entities
{
    public enum TypeEnum
    {
        Auto,
        Brommer,
        Fiets
    }
    public class Voertuig
    {
        public Guid Id { get; set; }
        [Required]
        public TypeEnum Type { get; set; }
        
        [Required]
        [RegularExpression(@"^[1-7]{1}[a-zA-Z]{3}(-)[0-9]{3}$")]
        public string Nummerplaat { get; set; }

        public ICollection<Bestelling> Bestellingen { get; set; }
    }
}
