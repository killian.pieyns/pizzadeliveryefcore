﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Entities
{
    public class StandaardPizza
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Naam { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal PrijsKlein { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal PrijsMedium { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal PrijsGroot { get; set; }
        [Required]
        [StringLength(255)]
        public string Beschrijving { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}
