﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Entities
{
    public class PizzaDeliveryContext : IdentityDbContext<
        User,
        Role,
        Guid,
        IdentityUserClaim<Guid>,
        UserRole,
        IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>,
        IdentityUserToken<Guid>>
    {
        public DbSet<Adres> Adressen { get; set; }

        public DbSet<Bestelling> Bestellingen { get; set; }

        public DbSet<Pizza> Pizzas { get; set; }

        public DbSet<PizzaTopping> PizzaToppings { get; set; }

        public DbSet<Topping> Toppings { get; set; }

        public DbSet<Voertuig> Voertuigen { get; set; }

        public DbSet<StandaardPizza> StandaardPizzas { get; set; }

        public DbSet<RefreshToken> RefreshTokens { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PizzaDeliveryContext"/> class.
        /// </summary>
        /// <param name="options">options for dbcontext</param>
        public PizzaDeliveryContext(DbContextOptions<PizzaDeliveryContext> options)
            : base(options)
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            if (builder == null) { throw new ArgumentNullException(nameof(builder)); }

            builder.Entity<RefreshToken>(x =>
            {
                x.HasOne(x => x.User)
                .WithMany(x => x.RefreshTokens)
                .HasForeignKey(x => x.UserId)
                .IsRequired();
            });

            builder.Entity<Role>(b =>
            {
                b.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

                b.Property(b => b.Omschrijving).IsRequired(false);
            });

            builder.Entity<User>(b =>
            {
                b.HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();
            });

            builder.Entity<StandaardPizza>(x =>
            {
                x.Property(x => x.PrijsGroot).HasColumnType("decimal(18,2)");
                x.Property(x => x.PrijsMedium).HasColumnType("decimal(18,2)");
                x.Property(x => x.PrijsKlein).HasColumnType("decimal(18,2)");
            });

            builder.Entity<Topping>(x =>
            {
                x.Property(x => x.Prijs).HasColumnType("decimal(18,2)");
            });

            builder.Entity<Adres>(x =>
            {
                x.HasOne(x => x.User)
                .WithOne(x => x.Adres)
                .HasForeignKey<Adres>(x => x.UserId)
                .IsRequired();
            });

            // Table with several FKs may have only one of them with cascade delete.
            // This is a restriction of MS SQL Server itself.
            // So, as soon as you need both FKs to have cascade -you should implement such "cleanup" in your code.
            // Set one(or both) FKs to DeleteBehavior.Restrict, and in your controller/service prior to removing the Entity
            // manually find and delete all related Entities
            // OnDelete defines what happens with the child/dependent when the parent/principal (foreign key) is deleted
            builder.Entity<Pizza>(x =>
            {
                x.HasOne(x => x.StandaardPizza)
                .WithMany(x => x.Pizzas)
                .HasForeignKey(x => x.StandaardPizzaId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

                x.HasOne(x => x.Bestelling)
                .WithMany(x => x.Pizzas)
                .HasForeignKey(x => x.BestellingId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<PizzaTopping>(x =>
            {
                x.HasOne(x => x.Topping)
                .WithMany(x => x.PizzaToppings)
                .HasForeignKey(x => x.ToppingId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

                x.HasOne(x => x.Pizza)
                .WithMany(x => x.PizzaToppings)
                .HasForeignKey(x => x.PizzaId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Bestelling>(x =>
            {
                x.HasOne(x => x.Klant)
               .WithMany(x => x.BestellingenKlant)
               .HasForeignKey(x => x.KlantId)
               .IsRequired()
               .OnDelete(DeleteBehavior.Restrict);

                x.HasOne(x => x.Bereider)
                .WithMany(x => x.BestellingenBereider)
                .HasForeignKey(x => x.BereiderId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

                x.HasOne(x => x.Vervoerder)
                .WithMany(x => x.BestellingenVervoerder)
                .HasForeignKey(x => x.VervoerderId)
                .OnDelete(DeleteBehavior.Restrict);

                x.HasOne(x => x.Voertuig)
                .WithMany(x => x.Bestellingen)
                .HasForeignKey(x => x.VoertuigId)
                .OnDelete(DeleteBehavior.Restrict);

                x.Property(x => x.VervoerderId).IsRequired(false);
                x.Property(x => x.VoertuigId).IsRequired(false);
            });

            // Indexes (te implementeren)
            builder.Entity<Voertuig>(x =>
            {
                x.HasIndex(x => new { x.Type, x.Nummerplaat })
                .IsUnique(true)
                .HasDatabaseName("UQ_VoertuigType_Nummerplaat");
            });

            builder.Entity<Topping>(x =>
            {
                x.HasIndex(x => new { x.Naam, x.Prijs })
                .IsUnique(true)
                .HasDatabaseName("UQ_ToppingNaam_Prijs");
            });

            builder.Entity<User>(x =>
            {
                x.HasIndex(x => new { x.Voornaam, x.Familienaam, x.Email})
                .IsUnique(true)
                .HasDatabaseName("UQ_UserVoornaam_Familienaam_Adres");
            });

            builder.Entity<Adres>(x =>
            {
                x.HasIndex(x => new { x.Straat, x.Huisnummer, x.Postcode })
                .IsUnique(true)
                .HasDatabaseName("UQ_AdresPostcode");
            });
        }
    }
}
