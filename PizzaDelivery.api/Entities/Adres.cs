﻿// <copyright file="Adres.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaDelivery.Api.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    public class Adres
    {
        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string Straat { get; set; }

        [Required]
        [StringLength(10)]
        public string Huisnummer { get; set; }

        [Required]
        [DataType(DataType.PostalCode)]
        public int Postcode { get; set; }

        [Required]
        [StringLength(50)]
        public string Gemeente { get; set; }

        public User User { get; set; }
    }
}
