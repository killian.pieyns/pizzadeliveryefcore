﻿// <copyright file="Pizza.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaDelivery.Api.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    public enum FormaatEnum
    {
        Klein,
        Medium,
        Groot
    }

    public class Pizza
    {
        public Guid Id { get; set; }

        [Required]
        [Range(0, 100)]
        public int Aantal { get; set; }

        public FormaatEnum Formaat { get; set; }

        [Required]
        public Guid BestellingId { get; set; }

        [Required]
        public Guid StandaardPizzaId { get; set; }

        public Bestelling Bestelling { get; set; }

        public StandaardPizza StandaardPizza { get; set; }

        public ICollection<PizzaTopping> PizzaToppings { get; set; }
    }
}
