﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PizzaDelivery.Api.Entities
{
    public class Role : IdentityRole<Guid>
    {
        [StringLength(50)]
        public string Omschrijving { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
