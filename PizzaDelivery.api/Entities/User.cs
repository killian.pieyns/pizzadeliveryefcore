﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PizzaDelivery.Api.Entities
{
    public class User : IdentityUser<Guid>
    {
        [Required]
        [StringLength(20)]
        public string Voornaam { get; set; }

        [Required]
        [StringLength(50)]
        public string Familienaam { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

        public ICollection<Bestelling> BestellingenBereider { get; set; }

        public ICollection<Bestelling> BestellingenKlant { get; set; }

        public ICollection<Bestelling> BestellingenVervoerder { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }

        public Adres Adres { get; set; }
    }
}
