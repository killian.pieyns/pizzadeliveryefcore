﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Entities
{
    public class Topping
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Naam { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        [DataType(DataType.Currency)]
        public decimal Prijs { get; set; }

        public ICollection<PizzaTopping> PizzaToppings { get; set; }
    }
}
