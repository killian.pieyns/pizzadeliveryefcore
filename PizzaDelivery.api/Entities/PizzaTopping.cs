﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Entities
{
    public class PizzaTopping
    {
        public Guid Id { get; set; }

        [Required]
        public Guid PizzaId { get; set; }

        [Required]
        public Guid ToppingId { get; set; }

        [RegularExpression(@"^[0-1]{1}$")]
        public int MetOfZonder { get; set; }

        public Pizza Pizza { get; set; }

        public Topping Topping { get; set; }
    }
}
