﻿// <copyright file="Bestelling.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaDelivery.Api.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    public class Bestelling
    {
        public Guid Id { get; set; }

        [Required]
        public Guid KlantId { get; set; }

        public Guid? BereiderId { get; set; }

        public Guid? VervoerderId { get; set; }

        public Guid? VoertuigId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime TijdstipBestelling { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime TijdstipLevering { get; set; }

        public User Klant { get; set; }

        public User Bereider { get; set; }

        public User Vervoerder { get; set; }

        public Voertuig Voertuig { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}