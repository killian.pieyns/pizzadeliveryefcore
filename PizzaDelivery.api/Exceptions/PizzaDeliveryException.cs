﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Exceptions
{
    public class PizzaDeliveryException : Exception
    {
        public PizzaDeliveryError PizzaDeliveryError { get; }

        public PizzaDeliveryException(string message, string sourceClass, string sourceMethod, string status)
            : base(message)
        {
            PizzaDeliveryError = new PizzaDeliveryError
            {
                Type = this.GetType().Name,
                Message = message,
                SourceClass = sourceClass,
                SourceMethod = sourceMethod,
                Status = status,
            };
        }
    }
}
