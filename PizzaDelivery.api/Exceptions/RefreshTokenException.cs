﻿namespace PizzaDelivery.Api.Exceptions
{
    public class RefreshTokenException : PizzaDeliveryException
    {
        public RefreshTokenException(
            string message,
            string sourceClass,
            string sourceMethod,
            string status) : base(message, sourceClass, sourceMethod, status)
        {
        }
    }
}
