﻿namespace PizzaDelivery.Api.Exceptions
{
    public class IdentityException : PizzaDeliveryException
    {
        public IdentityException(
            string message,
            string sourceClass,
            string sourceMethod,
            string status) : base(message, sourceClass, sourceMethod, status)
        {
        }
    }
}
