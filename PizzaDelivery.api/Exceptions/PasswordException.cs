﻿namespace PizzaDelivery.Api.Exceptions
{
    public class PasswordException : PizzaDeliveryException
    {
        public PasswordException(
            string message,
            string sourceClass,
            string sourceMethod,
            string status) : base(message, sourceClass, sourceMethod, status)
        {
        }
    }
}
