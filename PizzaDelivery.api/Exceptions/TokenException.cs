﻿namespace PizzaDelivery.Api.Exceptions
{
    public class TokenException : PizzaDeliveryException
    {
        public TokenException(
            string message,
            string sourceClass,
            string sourceMethod,
            string status) : base(message, sourceClass, sourceMethod, status)
        {
        }
    }
}
