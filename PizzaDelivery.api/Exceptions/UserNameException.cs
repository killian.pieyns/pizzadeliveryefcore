﻿namespace PizzaDelivery.Api.Exceptions
{
    public class UserNameException : PizzaDeliveryException
    {
        public UserNameException(
            string message,
            string sourceClass,
            string sourceMethod,
            string status) : base(message, sourceClass, sourceMethod, status)
        {
        }
    }
}
