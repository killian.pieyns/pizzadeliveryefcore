﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using PizzaDelivery.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDelivery.Api.Data
{
    public static class DbIninitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            PizzaDeliveryContext _context = serviceProvider.GetRequiredService<PizzaDeliveryContext>();
            UserManager<User> _userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            RoleManager<Role> _roleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();

            // Delete existing database and create new database with default test data
            // =======================================================================

            _context.Database.EnsureCreated();

            // Look for any roles
            // If no roles found then the db is empty
            // Fill the db with test data
            // ======================================

            if (_context.Roles.Any())
            {
                return;   // DB has been seeded
            }

            // Create roles
            // ============

            _ = _roleManager.CreateAsync(new Role { Name = "Administrator", Omschrijving = "Administrator" }).Result;
            _ = _roleManager.CreateAsync(new Role { Name = "Beheerder", Omschrijving = "Beheerder" }).Result;
            _ = _roleManager.CreateAsync(new Role { Name = "Klant", Omschrijving = "Klant" }).Result;
            _ = _roleManager.CreateAsync(new Role { Name = "Bereider", Omschrijving = "Bereider" }).Result;
            _ = _roleManager.CreateAsync(new Role { Name = "Vervoerder", Omschrijving = "Vervoerder" }).Result;

            // Create users
            // ============

            User administrator = new User
            {
                Voornaam = "Killian",
                Familienaam = "Pieyns",
                Email = "killian.pieyns@student.odisee.be",
                UserName = "killian.pieyns@student.odisee.be",
            };

            _ = _userManager.CreateAsync(administrator, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(administrator, new List<string> { "Administrator", "Beheerder", "Klant", "Bereider", "Vervoerder" }).Result;

            User beheerder = new User
            {
                Voornaam = "Ruben",
                Familienaam = "DOS",
                Email = "ruben.dos@hotmail.be",
                UserName = "ruben.dos@hotmail.be",
            };

            _ = _userManager.CreateAsync(beheerder, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(beheerder, new List<string> { "Beheerder", "Klant", "Bereider", "Vervoerder" }).Result;

            User deliveryguy = new User
            {
                Voornaam = "Mohammed",
                Familienaam = "Couscous",
                Email = "mohammed.couscous@hotmail.be",
                UserName = "mohammed.couscous@hotmail.be",
            };

            _ = _userManager.CreateAsync(deliveryguy, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(deliveryguy, new List<string> { "Vervoerder" }).Result;

            User pizzaman = new User
            {
                Voornaam = "Pizza",
                Familienaam = "Yolo",
                Email = "pizza.yolo@hotmail.be",
                UserName = "pizza.yolo@hotmail.be"
            };

            _ = _userManager.CreateAsync(pizzaman, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(pizzaman, new List<string> { "Bereider" }).Result;

            User klant = new User
            {
                Voornaam = "An",
                Familienaam = "Anas",
                Email = "an.anas@hotmail.be",
                UserName = "an.anas@hotmail.be"
            };

            _ = _userManager.CreateAsync(klant, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(klant, new List<string> { "Klant" }).Result;

            User klant2 = new User
            {
                Voornaam = "Tom",
                Familienaam = "Maat",
                Email = "tom.maat@hotmail.be",
                UserName = "tom.maat@hotmail.be"
            };

            _ = _userManager.CreateAsync(klant2, "_Azerty123").Result;
            _ = _userManager.AddToRolesAsync(klant2, new List<string> { "Klant" }).Result;

            // Create adressen
            // ===============
            Adres adresAdmin = new Adres { User = administrator, Straat = "H. Demolstraat", Huisnummer = "31", Gemeente = "Wemmel", Postcode = 1780 };
            Adres adresBeheerder = new Adres { User = beheerder, Straat = "Gasthuisstraat", Huisnummer = "146a", Gemeente = "Roosdaal", Postcode = 1760 };
            Adres adresDeliveryguy = new Adres { User = deliveryguy, Straat = "Veeartsenstraat", Huisnummer = "55", Gemeente = "Anderlecht", Postcode = 1070 };
            Adres adresPizzaman = new Adres { User = pizzaman, Straat = "Italielei", Huisnummer = "9", Gemeente = "Antwerpen", Postcode = 2000 };
            Adres adresKlant = new Adres { User = klant, Straat = "Drongenstationstraat", Huisnummer = "13", Gemeente = "Gent", Postcode = 9031 };
            Adres adresKlant2 = new Adres { User = klant2, Straat = "Gevangenislaan", Huisnummer = "69", Gemeente = "Kontich", Postcode = 2550 };

            _context.Adressen.AddRange(new List<Adres> { adresAdmin, adresBeheerder, adresDeliveryguy, adresKlant, adresKlant2, adresPizzaman });

            // Create voertuigen
            // =================
            Voertuig maseratti = new Voertuig { Nummerplaat = "1-ACS-426", Type = TypeEnum.Auto };
            _context.Voertuigen.Add(maseratti);

            Voertuig harley = new Voertuig { Nummerplaat = "1-ABS-346", Type = TypeEnum.Brommer };
            _context.Voertuigen.Add(harley);

            Voertuig tringtring = new Voertuig { Type = TypeEnum.Fiets, Nummerplaat = "0-aaa-000" };
            _context.Voertuigen.Add(tringtring);

            // Create toppings
            // ===============

            Topping caprese = new Topping { Naam = "Caprese", Prijs = 1.20M };
            _context.Toppings.Add(caprese);

            Topping diavolo = new Topping { Naam = "Diavolo", Prijs = 0.80M };
            _context.Toppings.Add(diavolo);

            Topping bresoala = new Topping { Naam = "Bresoala", Prijs = 0.80M };
            _context.Toppings.Add(bresoala);

            // Create standaarpizza
            // ====================

            StandaardPizza margarita = new StandaardPizza 
            { 
                Naam = "Margarita", 
                Beschrijving = "Tomatensaus, kaas, oregano",
                PrijsKlein = 5.40M,
                PrijsMedium = 6.00M,
                PrijsGroot = 6.60M
            };

            StandaardPizza bolognaise = new StandaardPizza
            {
                Naam = "Bolognaise",
                Beschrijving = "Tomatensaus, Gehakt, kaas, basilicum",
                PrijsKlein = 5.80M,
                PrijsMedium = 6.40M,
                PrijsGroot = 7.00M
            };

            StandaardPizza peperoni = new StandaardPizza
            {
                Naam = "Peperoni",
                Beschrijving = "Tomatensaus, salami, zongedroogde tomaten, kaas, basilicum",
                PrijsKlein = 6.00M,
                PrijsMedium = 6.60M,
                PrijsGroot = 7.20M
            };


            _context.StandaardPizzas.AddRange(new List<StandaardPizza> { margarita, bolognaise, peperoni });


            // Create bestelingen
            // ==================

            Bestelling bestelling1 = new Bestelling
            {
                Klant = klant,
                Bereider = pizzaman,
                Vervoerder = deliveryguy,
                Voertuig = tringtring,
                TijdstipBestelling = new DateTime(2021, 1, 14, 17, 30, 0, DateTimeKind.Local),
                TijdstipLevering = new DateTime(2021, 1, 14, 18, 0, 0, DateTimeKind.Local)
            };

            Bestelling bestelling2 = new Bestelling
            {
                Klant = klant2,
                Bereider = pizzaman,
                Vervoerder = beheerder,
                Voertuig = maseratti,
                TijdstipBestelling = new DateTime(2021, 1, 15, 17, 35, 0, DateTimeKind.Local),
                TijdstipLevering = new DateTime(2021, 1, 15, 17, 40, 30, DateTimeKind.Local)
            };

            Bestelling bestelling3 = new Bestelling
            {
                Klant = klant,
                Bereider = pizzaman,
                TijdstipBestelling = new DateTime(2021, 1, 15, 17, 30, 0, DateTimeKind.Local),
                TijdstipLevering = new DateTime(2021, 1, 15, 17, 45, 0, DateTimeKind.Local)
            };


            _context.Bestellingen.AddRange(new List<Bestelling> { bestelling1, bestelling2, bestelling3 });


            // Create pizzas
            // =============

            Pizza pizzaDiavolo = new Pizza
            {
                Aantal = 1,
                Bestelling = bestelling1,
                StandaardPizza = margarita,
                Formaat = FormaatEnum.Klein
            };

            Pizza pizzaBresoala = new Pizza
            {
                Aantal = 2,
                Bestelling = bestelling2,
                StandaardPizza = margarita,
                Formaat = FormaatEnum.Medium
            };

            Pizza pizzaPeper = new Pizza
            {
                Aantal = 1,
                Bestelling = bestelling2,
                StandaardPizza = peperoni,
                Formaat = FormaatEnum.Groot
            };

            Pizza pizzaMarga = new Pizza
            {
                Aantal = 1,
                Bestelling = bestelling3,
                StandaardPizza = margarita,
                Formaat = FormaatEnum.Medium
            };

            Pizza pizzaCaprese = new Pizza
            {
                Aantal = 1,
                Bestelling = bestelling3,
                StandaardPizza = margarita,
                Formaat = FormaatEnum.Groot
            };

            _context.Pizzas.AddRange(new List<Pizza> {
                pizzaMarga,
                pizzaCaprese,
                pizzaDiavolo,
                pizzaPeper,
                pizzaBresoala });

            // Create ontleningen
            // ==================

            PizzaTopping pizzatoppingCaprese = new PizzaTopping
            {
                Topping = caprese,
                Pizza = pizzaCaprese,
                MetOfZonder = 1
            };

            PizzaTopping pizzatoppingBresoala = new PizzaTopping
            {
                Topping = bresoala,
                Pizza = pizzaBresoala,
                MetOfZonder = 1
            };

            PizzaTopping pizzatoppingDiavolo = new PizzaTopping
            {
                Topping = diavolo,
                Pizza = pizzaDiavolo,
                MetOfZonder = 1
            };

            _context.PizzaToppings.AddRange(new List<PizzaTopping> {
                pizzatoppingBresoala,
                pizzatoppingCaprese,
                pizzatoppingDiavolo
            });

            _context.SaveChanges();
        }
    }
}
