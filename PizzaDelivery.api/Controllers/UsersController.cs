﻿// <copyright file="UsersController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaDelivery.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using PizzaDelivery.Api.Exceptions;
    using PizzaDelivery.Api.Repositories;
    using PizzaDelivery.models.RefreshTokens;
    using PizzaDelivery.models.Users;

    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository userRepository;

        public UsersController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Get a list of all users.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users
        ///
        /// </remarks>
        /// <returns>List of GetUserModel</returns>
        /// <response code="200">Returns the list of users</response>
        /// <response code="404">No users were found</response> 
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Beheerder")]
        public async Task<ActionResult<List<GetUserModel>>> GetUsers()
        {
            try
            {
                List<GetUserModel> users = await this.userRepository.GetUsers();

                return users;
            }
            catch (PizzaDeliveryException e)
            {
                return NotFound(e.PizzaDeliveryError);
            }
        }

        /// <summary>
        /// Get details of an user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/{id}
        ///
        /// </remarks>
        /// <param name="id"></param>     
        /// <returns>An GetUserModel</returns>
        /// <response code="200">Returns the user</response>
        /// <response code="404">The user could not be found</response> 
        /// <response code="400">The id is not a valid Guid</response> 
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = "Leerling")]
        public async Task<ActionResult<GetUserModel>> GetUser(string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid userId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "GetUser", "400");
                }

                GetUserModel user = await this.userRepository.GetUser(userId);

                return user;
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else if (e.PizzaDeliveryError.Status.Equals("403"))
                {
                    return new ObjectResult(e.PizzaDeliveryError)
                    {
                        StatusCode = (int)HttpStatusCode.Forbidden,
                    };
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        /// <summary>
        /// Creates an user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users
        ///     {
        ///        "Voornaam": "Yves",
        ///        "Familienaam": "Blancquaert",
        ///        "Email": "yves@blancquaert.com",
        ///        "Password": "_Azerty123",
        ///        "Roles": [
        ///            "Leerkracht"
        ///        ]
        ///     }
        ///
        /// </remarks>
        /// <param name="postUserModel">Usermodel user to create user</param>
        /// <returns>A newly created user</returns>
        /// <response code="201">Returns the newly created user</response>
        /// <response code="400">If something went wrong while saving into the database</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [AllowAnonymous]
        public async Task<ActionResult<GetUserModel>> PostUser(PostUserModel postUserModel)
        {
            try
            {
                GetUserModel user = await this.userRepository.PostUser(postUserModel);

                return this.CreatedAtAction(nameof(this.GetUser), new { id = user.Id }, user);
            }
            catch (PizzaDeliveryException e)
            {
                return this.BadRequest(e.PizzaDeliveryError);
            }
        }

        /// <summary>
        /// Updates an user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/users/{id}
        ///     {
        ///        "Voornaam": "Yves",
        ///        "Familienaam": "Blancquaert",
        ///        "Email": "yves@blancquaert.com",
        ///        "Roles": [
        ///            "Leerkracht"
        ///        ]
        ///     }
        ///
        /// </remarks>
        /// <param name="id">id of user.</param>
        /// <param name="putUserModel">model containing fields to change.</param>
        /// <response code="204">Returns no content.</response>
        /// <response code="404">The user could not be found.</response>
        /// <response code="400">The id is not a valid Guid or something went wrong while saving into the database.</response>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Leerling")]
        public async Task<IActionResult> PutUser(string id, PutUserModel putUserModel)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid userId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "PutUser", "400");
                }

                await this.userRepository.PutUser(userId, putUserModel);

                return this.NoContent();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        /// <summary>
        /// Updates an user password.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PATCH /api/users/{id}
        ///     {
        ///        "CurrentPassword": "_Azerty123",
        ///        "NewPassword": "Azerty123!"
        ///     }
        ///
        /// </remarks>
        /// <param name="id">id of user</param>
        /// <param name="patchUserModel">model containing information to change</param>
        /// <response code="204">Returns no content</response>
        /// <response code="404">The user could not be found</response>
        /// <response code="400">The id is not a valid Guid or the current password does not match or the new password is not conform the password rules</response>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [Authorize(Roles = "Leerling")]
        public async Task<IActionResult> PatchUser(string id, PatchUserModel patchUserModel)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid userId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "PatchUser", "400");
                }

                await this.userRepository.PatchUser(userId, patchUserModel);

                return this.NoContent();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        /// <summary>
        /// Deletes an user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/users/{id}
        ///
        /// </remarks>
        /// <param name="id">id of user</param>
        /// <response code="204">Returns no content</response>
        /// <response code="404">The user could not be found</response>
        /// <response code="400">The id is not a valid Guid</response>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Beheerder")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid userId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "DeleteUser", "400");
                }

                await this.userRepository.DeleteUser(userId);

                return this.NoContent();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        // JWT Action Methods
        // ==================

        /// <summary>
        /// Authenticates an user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/authenticate
        ///     {
        ///        "UserName": "blancquaert.yves@svsl.be",
        ///        "Password": "_Azerty123"
        ///     }
        ///
        /// </remarks>
        /// <param name="postAuthenticateRequestModel">model for authentication</param>
        /// <returns>Details of authenticated user, an JWT token and a refresh token</returns>
        /// <response code="200">Returns the authenticated user with tokens</response>
        /// <response code="401">Incorrect credentials</response>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PostAuthenticateResponseModel>> Authenticate(PostAuthenticateRequestModel postAuthenticateRequestModel)
        {
            try
            {
                PostAuthenticateResponseModel postAuthenticateResponseModel = await this.userRepository.Authenticate(postAuthenticateRequestModel, IpAddress());

                this.SetTokenCookie(postAuthenticateResponseModel.RefreshToken);

                return postAuthenticateResponseModel;
            }
            catch (PizzaDeliveryException e)
            {
                return this.Unauthorized(e.PizzaDeliveryError);
            }
        }

        /// <summary>
        /// Renew tokens.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/refresh-token
        ///
        /// </remarks>
        /// <returns>Details of authenticated user, a new JWT token and a new refresh token</returns>
        /// <response code="200">Returns the authenticated user with new tokens</response>
        /// <response code="401">Invalid refresh token</response>   
        [AllowAnonymous]
        [HttpPost("refresh-token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PostAuthenticateResponseModel>> RefreshToken()
        {
            try
            {
                string refreshToken = this.Request.Cookies["refreshToken"];

                PostAuthenticateResponseModel postAuthenticateResponseModel = await userRepository.RefreshToken(refreshToken, IpAddress());

                SetTokenCookie(postAuthenticateResponseModel.RefreshToken);

                return postAuthenticateResponseModel;
            }
            catch (PizzaDeliveryException e)
            {
                return Unauthorized(e.PizzaDeliveryError);
            }
        }

        /// <summary>
        /// Revoke token.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/revoke-token
        ///     {
        ///        "Token": "Some token"
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Disables a refresh token</response>
        /// <response code="400">No token present in body or cookie</response>   
        /// <response code="401">No user found with this token or refresh token is no longer active</response>   
        [HttpPost("revoke-token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize(Roles = "Beheerder")]
        public async Task<IActionResult> RevokeToken(PostRevokeTokenRequestModel postRevokeTokenRequestModel)
        {
            try
            {
                // Accept token from request body or cookie
                string token = postRevokeTokenRequestModel.Token ?? Request.Cookies["refreshToken"];

                if (string.IsNullOrEmpty(token))
                {
                    throw new RefreshTokenException("Refresh token is required.", this.GetType().Name, "RevokeToken", "400");
                }

                await userRepository.RevokeToken(token, IpAddress());

                return Ok();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("400"))
                {
                    return BadRequest(e.PizzaDeliveryError);
                }
                else
                {
                    return Unauthorized(e.PizzaDeliveryError);
                }
            }
        }

        /// <summary>
        /// Get a list of all refresh tokens of a user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/{id}/refresh-tokens
        ///
        /// </remarks>
        /// <returns>List of GetRefreshTokenModel</returns>
        /// <response code="200">Returns the list of refresh tokens</response>
        /// <response code="404">No refresh tokens were found</response> 
        /// <response code="400">The id is not a valid Guid</response> 
        [HttpGet("{id}/refresh-tokens")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = "Beheerder")]
        public async Task<ActionResult<List<GetRefreshTokenModel>>> GetUserRefreshTokens(string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid userId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "GetUserRefreshTokens", "400");
                }

                List<GetRefreshTokenModel> refreshTokens = await userRepository.GetUserRefreshTokens(userId);

                return refreshTokens;
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        // JWT helper methods
        // ==================
        private void SetTokenCookie(string token)
        {
            CookieOptions cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };

            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        private string IpAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                return Request.Headers["X-Forwarded-For"];
            }
            else
            {
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            }
        }
    }
}
