﻿// <copyright file="BestellingenController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PizzaDelivery.Api.Controllers
{
    // For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using PizzaDelivery.Api.Exceptions;
    using PizzaDelivery.Api.Repositories;
    using PizzaDelivery.models.Bestellingen;

    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api/users/{userId}/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class BestellingenController : ControllerBase
    {
        private readonly IBestellingRepository bestellingRepository;

        public BestellingenController(IBestellingRepository bestellingRepository)
        {
            this.bestellingRepository = bestellingRepository;
        }

        // GET api/users/ontleningen
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = "Klant, Bereider")]
        public async Task<ActionResult<List<GetBestellingModel>>> GetBestellingen(string userId)
        {
            try
            {
                if (!Guid.TryParse(userId, out Guid validUserId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "GetBestellingen", "400");
                }

                List<GetBestellingModel> bestellingen = await this.bestellingRepository.GetBestellingen(validUserId);

                return bestellingen;
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else if (e.PizzaDeliveryError.Status.Equals("403"))
                {
                    return new ObjectResult(e.PizzaDeliveryError)
                    {
                        StatusCode = (int)HttpStatusCode.Forbidden,
                    };
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        // GET /api/users/bestellingen/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = "Klant, Bereider")]
        public async Task<ActionResult<GetBestellingModel>> GetBestelling(string userId, string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId) || !Guid.TryParse(userId, out Guid validUserId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "GetBestelling", "400");
                }

                GetBestellingModel bestelling = await this.bestellingRepository.GetBestelling(validUserId, bestellingId);

                return bestelling;
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else if (e.PizzaDeliveryError.Status.Equals("403"))
                {
                    return new ObjectResult(e.PizzaDeliveryError)
                    {
                        StatusCode = (int)HttpStatusCode.Forbidden,
                    };
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        // POST api/<BestellingController>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = "Klant")]
        public async Task<ActionResult<GetBestellingModel>> PostBestelling(string userId, PostBestellingModel postBestellingModel)
        {
            try
            {
                if (!Guid.TryParse(userId, out Guid validUserId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "PostBestelling", "400");
                }

                GetBestellingModel bestelling = await this.bestellingRepository.PostBestelling(validUserId, postBestellingModel);

                return this.CreatedAtAction(nameof(this.GetBestelling), new { id = bestelling.Id }, bestelling);
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        //// PUT api/<BestellingController>/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Bereider")]
        public async Task<IActionResult> PutBestelling(string userId, string id, PutBestellingModel putBestellingModel)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId) || !Guid.TryParse(userId, out Guid validUserId))
                {
                    throw new GuidException("Invalid Guid format", this.GetType().Name, "PutBestelling", "400");
                }

                await this.bestellingRepository.PutBestelling(validUserId, bestellingId, putBestellingModel);

                return this.NoContent();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }

        // DELETE api/<BestellingController>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Bereider")]
        public async Task<IActionResult> DeleteBestelling(string userId, string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId) || !Guid.TryParse(userId, out Guid validUserId))
                {
                    throw new GuidException("Invalid Guid format.", this.GetType().Name, "DeleteBestelling", "400");
                }

                await this.bestellingRepository.DeleteBestelling(validUserId, bestellingId);

                return this.NoContent();
            }
            catch (PizzaDeliveryException e)
            {
                if (e.PizzaDeliveryError.Status.Equals("404"))
                {
                    return this.NotFound(e.PizzaDeliveryError);
                }
                else
                {
                    return this.BadRequest(e.PizzaDeliveryError);
                }
            }
        }
    }
}
